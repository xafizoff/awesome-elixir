defmodule Awesome.Repo.Migrations.AddStarCountIndex do
  use Ecto.Migration

  def change do
    create index(:projects, [:star_count])
  end
end