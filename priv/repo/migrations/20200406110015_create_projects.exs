defmodule Awesome.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :name, :string
      add :description, :string
      add :url, :string
      add :star_count, :integer
      add :pushed_at, :naive_datetime
      add :group_id, references(:groups, on_delete: :nothing)

      timestamps()
    end

    create index(:projects, [:group_id])
    create unique_index(:projects, [:name])
  end
end
