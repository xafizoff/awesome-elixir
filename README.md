# Awesome Elixir List

Mirror of [h4cc/awesome-elixir](https://github.com/h4cc/awesome-elixir)

# Building and starting

* In OS shell: `$ mix deps.get && iex -S mix phx.server`
* To fetch the Awesome List initially, execute in IEx: `> Awesome.Worker.fetch`. Awesome List will be updated every day at UTC midnight

# Access to the GitHub API

In order to fetch repo data from GitHub, we need to query GitHub API. But unauthorized request have very low rate limit (1 request per minute).
To increase that limit you can set application config `:awesome, :github_auth` (see `config.exs`). It consists of your GitHub login and your
personal access token delimited with colon (`:`). You can generate personal access tokens on your GitHub account settings page, in the `Developer Settings`
section. If you set the `github_auth` setting to an empty list, then unauthorized requests with 1 minute delay will be sent.
Note that the setting value MUST be a charlist, not binary.

# Limitations

* Projects from GitHub only will be fetched
