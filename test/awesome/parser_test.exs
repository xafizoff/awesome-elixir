defmodule Awesome.ParserTest do

  use ExUnit.Case

  @text "
Starting text

## Group 1
*Group 1 description*

* [owner1/project1](https://github.com/owner1/project1) - Project 1 from Owner 1.
* [owner2/project2](https://github.com/owner2/project2) - Project 2 from Owner 2.

## Group 2
* Group 2 description*

* [project](https://github.com/owner/project) - Project from owner

# Ending text
"

  test "awesome list parser test" do
    {:ok, [group1, group2], "", %{}, _, _} = Awesome.Parser.awesome_list(@text)
    %{name: name1, description: descr1, projects: [proj1, proj2]} = group1
    %{name: name2, description: descr2, projects: [proj3]} = group2
    assert name1 == "Group 1"
    assert descr1 == "Group 1 description"
    assert name2 == "Group 2"
    assert descr2 == " Group 2 description"
    assert proj1[:name] == "owner1/project1"
    assert proj1[:url] == "https://github.com/owner1/project1"
    assert proj1[:description] =~ "Project 1 from Owner 1"
    assert proj2[:name] == "owner2/project2"
    assert proj3[:name] == "project"
  end

  test "extract owner and repo from github project url" do
    {:ok, project, "", %{}, _, _} = Awesome.Parser.owner_and_repo("https://github.com/h4cc/awesome-elixir")
    assert project == 'h4cc/awesome-elixir'
  end

end
