defmodule AwesomeWeb.PageControllerTest do
  use AwesomeWeb.ConnCase

  alias Awesome.{Repo, Group, Project}

  setup do
    Repo.insert(%Group{id: 1, name: "Group 1", description: "Group 1 description"})
    Repo.insert(%Project{group_id: 1, id: 1, name: "Project 1", description: "Project 1 description", star_count: 10, 
                         last_update: ~N[2020-05-05 20:00:00], url: "http://project1.com"})
    Repo.insert(%Project{group_id: 1, id: 2, name: "Project 2", description: "Project 2 description", star_count: 2, 
                         last_update: ~N[2020-05-03 20:00:00], url: "http://project2.com"})
    Repo.insert(%Group{id: 2, name: "Group 2", description: "Group 2 description"})
    Repo.insert(%Project{group_id: 2, id: 3, name: "Project 3", description: "Project 3 description", star_count: 100, 
                         last_update: ~N[2020-05-01 20:00:00], url: "http://project3.com"})
    {:ok, conn: build_conn()}
  end

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "<li>"
    assert html_response(conn, 200) =~ "Group 1"
    assert html_response(conn, 200) =~ "<a href=\"http://project1.com\">Project 1</a> ⭐ 10 "
    assert html_response(conn, 200) =~ "<a href=\"http://project2.com\">Project 2</a> ⭐ 2 "
    assert html_response(conn, 200) =~ "Group 2"
    assert html_response(conn, 200) =~ "<a href=\"http://project3.com\">Project 3</a> ⭐ 100 "
  end

  test "GET /?min_stars=10", %{conn: conn} do
    conn = get(conn, "/?min_stars=10")
    assert html_response(conn, 200) =~ "<li>"
    assert html_response(conn, 200) =~ "Group 1"
    assert html_response(conn, 200) =~ "Project 1"
    refute html_response(conn, 200) =~ "Project 2"
  end

  test "GET /?min_stars=11", %{conn: conn} do
    conn = get(conn, "/?min_stars=11")
    assert html_response(conn, 200) =~ "<li>"
    refute html_response(conn, 200) =~ "Group 1"
    assert html_response(conn, 200) =~ "Group 2"
    assert html_response(conn, 200) =~ "Project 3"
  end

  test "GET /?min_stars=101", %{conn: conn} do
    conn = get(conn, "/?min_stars=101")
    refute html_response(conn, 200) =~ "<li>"
  end

  test "GET /?min_stars=not_an_integer", %{conn: conn} do
    conn = get(conn, "/?min_stars=not_an_integer")
    assert html_response(conn, 200) =~ "<li>"
    assert html_response(conn, 200) =~ "Group 1"
    assert html_response(conn, 200) =~ "Project 1"
    assert html_response(conn, 200) =~ "Project 2"
    assert html_response(conn, 200) =~ "Group 2"
    assert html_response(conn, 200) =~ "Project 3"
  end
end
