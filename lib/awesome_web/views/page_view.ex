defmodule AwesomeWeb.PageView do
  use AwesomeWeb, :view

  def as_html(markdown) do
    raw(String.replace(Earmark.as_html!(markdown), "p>", "span>"))
  end
end
