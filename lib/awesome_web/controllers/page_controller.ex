defmodule AwesomeWeb.PageController do
  use AwesomeWeb, :controller
  alias Awesome.Repo

  def index(conn, params) do
    stars = get_stars(params)
    groups = Repo.get_groups(stars)
    render(conn, "index.html", groups: groups)
  end

  defp get_stars(%{"min_stars" => min_stars}) do
    case Integer.parse(min_stars) do
      :error -> 0
      {val, _} -> val
    end
  end
  defp get_stars(_), do: 0

end
