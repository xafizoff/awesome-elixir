defmodule Awesome.Repo do
  use Ecto.Repo,
    otp_app: :awesome,
    adapter: Ecto.Adapters.Postgres

  alias Awesome.{Group, Project}
  import Ecto.Query

  def get_groups(stars) do
    q1 = from g in Group,
            join: p in assoc(g, :projects),
            where: p.star_count >= ^stars,
            order_by: [g.name],
            group_by: [g.id, g.name, g.description],
            select: %{id: g.id, name: g.name, description: g.description}
    groups = all(q1)
    q2 = from p in Project,
            where: p.star_count >= ^stars,
            select: p
    projects = all(q2)
    Enum.map(groups, fn g -> Map.put(g, :projects, Enum.filter(projects, fn p -> p.group_id == g.id end)) end)
  end
end
