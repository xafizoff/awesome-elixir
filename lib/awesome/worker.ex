defmodule Awesome.Worker do
  use GenServer
  require Logger
  alias Awesome.{Repo, Group, Project, Parser, Github}

  def start_link(_arg), do: GenServer.start_link(__MODULE__, [], name: __MODULE__)
  def stop, do: GenServer.stop(__MODULE__)
  def fetch, do: GenServer.cast(__MODULE__, :once)
  def next, do: GenServer.cast(__MODULE__, :next)

  # Callbacks

  @impl true
  def init([]) do
    fetch_at_midnight()
    {:ok, []}
  end

  @impl true
  def handle_info(message, state) do
    handle_cast(message, state)
  end

  @impl true
  def handle_cast(:fetch, state) do
    Process.send_after(__MODULE__, :fetch, 24*60*60*1000)
    handle_cast(:once, state)
  end

  @impl true
  def handle_cast(:once, [_|_] = state) do
    Logger.warn "Fetch already in progress"
    {:noreply, state}
  end

  @impl true
  def handle_cast(:once, _state) do
    if Github.authorized? do
      Logger.debug "Performing authorized requests to the GitHub API"
    else
      Logger.debug "Performing unauthorized request to the GitHub API"
    end
    case Github.fetch do
      {:ok, data} ->
        next()
        {:noreply, data}
      _ ->
        {:noreply, []}
    end
  end

  @impl true
  def handle_cast(:next, []) do
    Logger.debug "Fetch finished"
    {:noreply, []}
  end

  @impl true
  def handle_cast(:next, [head|tail]) do
    next()
    upsert_group!(head)
    {:noreply, tail}
  end

  defp fetch_at_midnight do
    now = NaiveDateTime.utc_now
    midnight = %{ %{ %{ now | hour: 23 } | minute: 59 } | second: 59 }
    diff = NaiveDateTime.diff(midnight, now)
    Process.send_after(__MODULE__, :fetch, diff * 1000)
  end

  defp upsert_group!(group) do
    Logger.debug("Upserting group " <> group[:name])
    new_group = Repo.insert!(
      %Group{name: group[:name], description: group[:description]},
      on_conflict: [set: [description: group[:description]]],
      conflict_target: :name)
    upsert_projects!(new_group.id, group[:projects])
  end

  def upsert_projects!(_, []), do: :skip
  def upsert_projects!(group_id,[head|tail]) do
    case Parser.owner_and_repo(head[:url]) do
      {:ok, owner_and_repo, _, _, _, _} ->
        case Github.get_repo(owner_and_repo) do
          {:ok, repo} ->
            Logger.debug("Upserting project " <> head[:name] <> " : " <> List.to_string(owner_and_repo))
            Repo.insert!(
              %Project{group_id: group_id, url: head[:url], name: head[:name], description: head[:description],
                       star_count: repo[:star_count], pushed_at: repo[:pushed_at]
                      },
              on_conflict: [set: [description: head[:description],
                                  star_count: repo[:star_count],
                                  pushed_at: repo[:pushed_at]]],
              conflict_target: :name)
          unexpected ->
            Logger.warn owner_and_repo
            unexpected |> inspect |> Logger.warn end
      _ -> :skip end
    if not Github.authorized? do
      Process.sleep(60000)
    end
    upsert_projects!(group_id, tail)
  end

end
