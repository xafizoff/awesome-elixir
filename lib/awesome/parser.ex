defmodule Awesome.Parser do
  import NimbleParsec

  @to_str {List, :to_string, []}
  @space 32
  @new_line 10

  defp to_group([a,b|c]), do: %{name: a, description: b, projects: c}
  defp to_project([a,b,c]), do: %{name: a, url: b, description: c}

  till_star = repeat(utf8_char([]) |> lookahead_not(string("*"))) |> utf8_char([])

  till_sharp = repeat(utf8_char([]) |> lookahead_not(string("#"))) |> utf8_char([])

  till_bracket = repeat(utf8_char([]) |> lookahead_not(string(")"))) |> utf8_char([])

  till_square_bracket = repeat(utf8_char([]) |> lookahead_not(string("]"))) |> utf8_char([])

  whites = repeat(utf8_char([@space, @new_line]))

  head = whites |> choice([string("## "),string("# "),empty()])

  pre = repeat(utf8_char([]) |> lookahead_not(string("##"))) |> utf8_char([]) |> reduce(@to_str)

  post = repeat(utf8_char([])) |> reduce(@to_str)

  title = repeat(utf8_char([]) |> lookahead_not(string("\n"))) |> utf8_char([]) |> reduce(@to_str)

  descr = till_star |> reduce(@to_str)

  proj_name = till_square_bracket |> reduce(@to_str)

  url = till_bracket |> reduce(@to_str)

  proj_descr = repeat(utf8_char([]) |> lookahead_not(string("\n"))) |> utf8_char([]) |> ignore(string("\n")) |> reduce(@to_str)

  defparsec :project,
    ignore(string("* [")) |> concat(proj_name) |> ignore(string("](")) |> concat(url) |> ignore(string(") - ")) |> concat(proj_descr) |> reduce({:to_project, []})

  body = ignore(till_star) |> repeat(parsec(:project)) |> ignore(till_sharp |> reduce(@to_str))

  defparsec :group,
    ignore(head)
    |> concat(title)
    |> ignore(till_star)
    |> ignore(string("*"))
    |> concat(descr)
    |> ignore(string("*"))
    |> concat(body)
    |> reduce({:to_group, []})

  defparsec :awesome_list,
    ignore(pre)
    |> repeat(parsec(:group))
    |> ignore(post)

  @chars [?-, ?_, ?., ?0..?9, ?a..?z, ?A..?Z]

  defparsec :owner_and_repo,
    ignore(string("https://github.com/"))
    |> repeat(ascii_char(@chars))
    |> ascii_char([?/])
    |> repeat(ascii_char(@chars))
    |> ignore(eos())

end
