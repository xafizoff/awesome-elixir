defmodule Awesome.Github do
  require Logger

  alias Awesome.Parser

  def fetch do
    case :httpc.request(:get, {'https://raw.githubusercontent.com/h4cc/awesome-elixir/master/README.md', []}, [], []) do
      {:ok, {{_, 200, _}, _, data}} ->
        case Parser.awesome_list(List.to_string(data)) do
          {:ok, parsed, _, _, _, _} -> {:ok, parsed}
          error -> error
        end
      unexpected -> unexpected
    end
  end

  def get_repo(owner_repo), do: get_repo(Application.get_env(:awesome, :github_auth), owner_repo)

  def authorized? do
    auth = Application.get_env(:awesome, :github_auth)
    is_list(auth) && length(auth) > 3
  end

  def get_repo(auth, owner_repo) do
    case :httpc.request(:get,
                        {'https://api.github.com/repos/' ++ owner_repo,
                        [{'user-agent', 'erlang httpc'},
                         {'Authorization', 'Basic ' ++ String.to_charlist(:base64.encode(auth))}]}, [], []) do
      {:ok, {{_, 200, _}, _, data}} ->
        {:ok, decoded} = Jason.decode(data)
        {:ok, pushed_at} = NaiveDateTime.from_iso8601(decoded["pushed_at"])
        {:ok, %{star_count: decoded["stargazers_count"], pushed_at: pushed_at}}
      unexpected -> {:error, unexpected}
    end
  end

end
