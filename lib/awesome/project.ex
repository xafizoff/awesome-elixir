defmodule Awesome.Project do
  use Ecto.Schema
  import Ecto.Changeset

  schema "projects" do
    field :description, :string
    field :pushed_at, :naive_datetime
    field :name, :string
    field :star_count, :integer
    field :url, :string
    belongs_to :group, Awesome.Group

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :description, :url, :star_count, :pushed_at])
    |> validate_required([:name])
  end
end
